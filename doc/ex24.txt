// This source code is subject to the terms of the Mozilla Public License 2.0 at https://mozilla.org/MPL/2.0/
// © NattapatAttiratanasunthron

//@version=5
indicator(title="volumeProfile")

var k_value = array.new<float>(100, na)
var k_index = array.new<float>(100, na)
// กำหนดรายการข้อมูลที่ต้องการให้ลูป

var pass = 0
var indexat = 0

// กำหนดวันที่เริ่มต้นของกราฟ (เป็นวันที่ 1 มกราคม 2023)
custom_start_date = input.time(title="Start Date", defval=timestamp("2023-01-01"))
var float adjustedPrice1 = na
var float adjustedVolume1 = na

 
if (time > custom_start_date)
    // Calculate the adjusted price
    adjustedPrice1 := close
    adjustedVolume1 := (volume/math.max(volume,bar_index))


pass := 0
for j = 0 to indexat
    if adjustedPrice1 == array.get(k_index,j)
        pass := 1
        float v = array.get(k_value,j) 
        float v1 = v + adjustedVolume1
        array.set(k_value,j,v1 ) 
    
if pass == 0 and adjustedPrice1 >=0
    array.set(k_index,indexat, adjustedPrice1)
    array.set(k_value,indexat, adjustedVolume1) 
    indexat:= indexat+1


plot(adjustedPrice1)
//plot(adjustedVolume1,style = plot.style_histogram)

if barstate.islast
    for i = 0 to indexat 
        line.new(x1=bar_index,
                 y1=array.get(k_index,i), 
                 
                 x2=bar_index[array.get(k_value,i)],

                 y2=array.get(k_index,i),
                 color=color.gray ,style  = line.style_solid,
                 width = 2)

     
        //if i == indexat
        //    label.new(text=str.tostring(array.get(k_value,i))+'@'+str.tostring(array.get(k_index,i))+'@'+str.tostring(indexat)   ,x=bar_index+20,y = array.get(k_index,i),color = color.white)



