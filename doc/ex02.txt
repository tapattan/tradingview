// This source code is subject to the terms of the Mozilla Public License 2.0 at https://mozilla.org/MPL/2.0/
// © NattapatAttiratanasunthron

//@version=5
indicator("My script","MS",false,format = format.percent)
plot(close,color=color.red)
plot(close,color=color.green,show_last = 50,style = plot.style_circles,linewidth = 2)