// Mark Minervini - Trend Template
// This source code is subject to the terms of the Mozilla Public License 2.0 at https://mozilla.org/MPL/2.0/
// © NattapatAttiratanasunthron

//@version=5
indicator("Mark Minervini - Trend Template",format = format.inherit,overlay = false)
 
sma50 = ta.sma(close, 50)
sma150 = ta.sma(close, 150)
sma200 = ta.sma(close, 200)

plot(close,title = 'close price')
plot(sma50 ,title = 'SMA50',color = color.purple)
plot(sma150 ,title = 'SMA150',color = color.green)
plot(sma200,title = 'SMA200',color = color.blue)

// 1. stock price is above ma 150 and 200
is_price_above_sma_150_and_200 = if (close > sma150) and (close > sma200)
    true 
else 
    false


// 2. sma 150 is above sma 200
is_sma_150_above_sma_200 = if sma150 > sma200 
    true
else 
    false

// 3. sma 200 is trending at least 1 month(22 days)
is_trending_at_least_1_month = if sma200 > sma200[22]  //ย้อนหลัง 1 เดือน 
    true
else 
    false

// 4. sma 50 is above both sma 150 and 200
is_sma_50_above_sma_150_and_200 = if (sma50 > sma150) and (sma50 > sma200)
    true 
else 
    false

// 5.
highest_price = ta.highest(high, 252)
lowest_price = ta.lowest(low, 252)

plot(highest_price,color = color.orange,title = 'high52week')
plot(lowest_price,color = color.orange,title = 'high52week')

is_price_25_percent_above_52_weeks_low = if (close/lowest_price)-1 > 0.25
    true 
else 
    false


// 6.
is_price_within_52_high = if (highest_price/close)-1 < 0.25
    true 
else 
    false


//8.close > SMA50
price_above_SMA50 = if (close > sma50) 
    true 
else 
    false
 

var T = table.new(
     position = position.bottom_right, columns=2, rows=10, bgcolor=color.white, 
     frame_color=color.gray, frame_width=2, border_color=color.gray, border_width=1)

if barstate.islast
    
    light1 = if is_price_above_sma_150_and_200
        color.green
    else
        color.red

    table.cell(table_id=T, column=0, row= 1, text='(1)ราคาหุ้นยืนเหนือเส้นค่าเฉลี่ย MA50 และ MA200', width=0, height=0, text_color=color.black, text_size=size.normal, bgcolor=color.white)
    table.cell(table_id=T, column=1, row= 1, text= str.tostring(is_price_above_sma_150_and_200), width=0, height=0, text_color=color.black, text_size=size.normal, bgcolor=light1)

    light2 = if is_sma_150_above_sma_200
        color.green
    else
        color.red

    table.cell(table_id=T, column=0, row= 2, text='(2)เส้น MA150 ยืนเหนือเส้น MA200', width=0, height=0, text_color=color.black, text_size=size.normal, bgcolor=color.white)
    table.cell(table_id=T, column=1, row= 2, text= str.tostring(is_sma_150_above_sma_200), width=0, height=0, text_color=color.black, text_size=size.normal, bgcolor=light2)
    
    light3 = if is_trending_at_least_1_month
        color.green
    else
        color.red

    table.cell(table_id=T, column=0, row= 3, text='(3) SMA200 เป็นขาขึ้นอย่างน้อย 1 เดือน', width=0, height=0, text_color=color.black, text_size=size.normal, bgcolor=color.white)
    table.cell(table_id=T, column=1, row= 3, text= str.tostring(is_trending_at_least_1_month), width=0, height=0, text_color=color.black, text_size=size.normal, bgcolor=light3)
    
    light4 = if is_sma_50_above_sma_150_and_200
        color.green
    else
        color.red 

    table.cell(table_id=T, column=0, row= 4, text='(4) SMA50 ยืนเหนือ SMA150 และ SMA200', width=0, height=0, text_color=color.black, text_size=size.normal, bgcolor=color.white)
    table.cell(table_id=T, column=1, row= 4, text= str.tostring(is_sma_50_above_sma_150_and_200), width=0, height=0, text_color=color.black, text_size=size.normal, bgcolor=light4)
    
    light5 = if is_price_25_percent_above_52_weeks_low
        color.green
    else
        color.red 

    table.cell(table_id=T, column=0, row= 5, text='(5) ราคายืนเหนือราคาต่ำสุด 52week เกิน25%', width=0, height=0, text_color=color.black, text_size=size.normal, bgcolor=color.white)
    table.cell(table_id=T, column=1, row= 5, text= str.tostring(is_price_25_percent_above_52_weeks_low) +' '+str.tostring(math.round(((close/lowest_price)-1)*100,2))+'%', width=0, height=0, text_color=color.black, text_size=size.normal, bgcolor=light5)
    
    light6 = if is_price_within_52_high
        color.green
    else
        color.red 

    table.cell(table_id=T, column=0, row= 6, text='(6) ราคายืนใกล้ราคาสูงสุด 52week ไม่ต่ำกว่า25%', width=0, height=0, text_color=color.black, text_size=size.normal, bgcolor=color.white)
    table.cell(table_id=T, column=1, row= 6, text= str.tostring(is_price_within_52_high) +' '+str.tostring(math.round(((highest_price/close)-1)*100,2))+'%', width=0, height=0, text_color=color.black, text_size=size.normal, bgcolor=light6)


    table.cell(table_id=T, column=0, row= 7, text='(7) RSI สูงกว่า 70', width=0, height=0, text_color=color.black, text_size=size.normal, bgcolor=color.white)
    table.cell(table_id=T, column=1, row= 7, text= str.tostring('N/A เขียนไม่ได้'), width=0, height=0, text_color=color.black, text_size=size.normal, bgcolor=color.white)
    
    
    light8 = if price_above_SMA50
        color.green
    else
        color.red 

    table.cell(table_id=T, column=0, row= 8, text='(8) ราคาหุ้นยืนเหนือเส้นค่าเฉลี่ย SMA50', width=0, height=0, text_color=color.black, text_size=size.normal, bgcolor=color.white)
    table.cell(table_id=T, column=1, row= 8, text= str.tostring(price_above_SMA50), width=0, height=0, text_color=color.black, text_size=size.normal, bgcolor=light8)