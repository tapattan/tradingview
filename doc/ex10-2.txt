//@version=5
indicator("First Day Price Example", overlay = true)

// กำหนดจำนวนรอบที่ต้องการให้ลูปทำงาน
numBars = 4
// ใช้ for เพื่อวาดเส้นแนวนอนบนแผนภูมิ
for i = 0 to numBars
    line.new(0,i,bar_index,i)


// กำหนดรายการข้อมูลที่ต้องการให้ลูป
var mySeries = array.from('ant','bird','cat','dog')

// วนลูปในรายการ mySeries
for i = 0 to array.size(mySeries)-1
    label.new(x=bar_index, y=i, text=str.tostring(array.get(mySeries, i)),  color=color.red)